Drupal.behaviors.mtgfilterBehavior = function(context) {
  $('a.mtgfilter-cardbox', context).bind('mouseover', function() {
    var $this = $(this),
      id = $this.parents('.mtgfilter:eq(0)').attr('id');
      
      $('#'+id+'_box').show().attr('src', $.mtgFilter.getSource($this.html()));
  }) // end $.bind()
  .bind('click', function() {
    return false;
  }); // end $.bind()
  
  $('div.mtgfilter-tabs li a', context).bind('click', function() {
    var $this = $(this),
      $parent = $this.parents('li:eq(0)'),
      $container = $parent.parents('div.mtgfilter:eq(0)'),
      tabIndex = $parent.attr('id').match(/mtgfilter-.*?-tab-(\d)/)[1];
    
    // de-select any currently selected tabs
    $container.find('div.mtgfilter-tabs li.mtgfilter-active')
      .removeClass('mtgfilter-active');
    
    // select the currently selected tab
    $parent.addClass('mtgfilter-active');
    
    // hide any currently selected tabs
    $container.find('div.mtgfilter-content div.mtgfilter-active')
        .removeClass('mtgfilter-active')
        .addClass('mtgfilter-hidden');
    
    // select the appropriate tab
    $container
      .find('div.mtgfilter-content div.mtgfilter-tabcontent:eq('+tabIndex+')')
        .removeClass('mtgfilter-hidden')
        .addClass('mtgfilter-active');
  }); // end $.bind()
  
  // bind the click event to the toggle link
  $('.mtgfilter-toggle', context).bind('click', function() {
    var $parent = $(this).parents('.mtgfilter:eq(0)');
    
    // toggle the content and tabs
    $('.mtgfilter-tabs, .mtgfilter-content', $parent).slideToggle('slow');
  }); // end $.bind()
}; // end function Drupal.behaviors.mtgfilterBehavior()

$.mtgFilter = {
  /**
   * Returns the url for a card based on its name
   * 
   * @param {String} name
   * @return {String}
   */
  getSource: function(name) {
    name = name.replace(/[,-]/g, ' ').replace(/'/g, '').replace(/[ ]+/g, ' ').replace(/ /g, '_');
    name = $('<span>').text(name).html();
    name = $.URLEncode(name);
    
    return 'http://www.wizards.com/global/images/magic/general/'+name+'.jpg';
  } // end function getSource()
};


